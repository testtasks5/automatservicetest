


namespace StockApp.Data;

public class Products
{
    private int count;
    public int Count 
    {   get { return count; }
        set
        {
            if(value < 1)
                count = 1;
            else
                count = value;
            NotifyStateChanged();
        }
    }
    public string Name { get; set; }
    public string Manufacturer { get; set; }
    private void NotifyStateChanged() => OnChange?.Invoke();

    public event Action OnChange = default!;

    public Products()
    {
        Count = 1;
        Name = string.Empty;
        Manufacturer = string.Empty;
    }

    public void Up()
    {
        Count++;
        NotifyStateChanged();
    }
    public void Down()
    {
        Count--;
        NotifyStateChanged();
    }
}