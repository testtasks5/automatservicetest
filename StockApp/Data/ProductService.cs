

namespace StockApp.Data;

public class ProductService
{
    public Products[] Products {get; set; }
    private static readonly string[] someProducts = new[]
    {
        "SomeProduct1", "SomeProduct2", "SomeProduct3", "SomeProduct4", "SomeProduct5" 
    };

    private static readonly string[] someManufature = new[]
    {
        "ManufatureRus", "ManufatureUsa", "ManufatureGer"
    };
    public ProductService()
    {
        Products = GetProducts();
    }

    public Products[] GetProducts()
    {
        return (Enumerable.Range(1,5).Select(product => new Products 
        {
            Name = someProducts[Random.Shared.Next(someProducts.Length)],
            Manufacturer = someManufature[Random.Shared.Next(someManufature.Length)] 
        }).ToArray());
    }
}